---
title: "Ablative to begin offering Multihop Virtual Private Servers, Multihop Only Free Web Hosting and Bring Your Own .onion"
date: 2021-01-01T00:00:01Z
tags:
- multihop
- free
- irc
- quadhop
---
When Ablative.Hosting first launched in 2017 the plan was to provide clearnet website operators
the option to also have a .onion with little to no fuss _(along with the usual features of free SSL, managed
patching etc etc)_.

The takeup of [Single Hop](https://ablative.hosting/single-hop-onion-hosting/) or [Shared Single Hop](https://ablative.hosting/shared-single-hop-onion-hosting/) accounts pales in
comparison to the constant battle to keep a stock of hypervisor servers provisioned to keep pace with the demand for [Multihop Hosting](https://ablative.hosting/multi-hop-onion-hosting/) accounts.

Following the loss of Daniels Hosting we introduced our Free Tier [Ablative.Website](https://ablative.website), whilst this was warmly received the #1 request was to not have a clearnet domain.

Reflecting on the past few years of operation I realised that **I** wouldn't purchase managed web hosting _(preferring instead to configure my own server)_ and that the clearnet is too susceptible to censorship / surveillance.

With this in mind; Ablative Hosting is changing.

## Multihop Virtual Private Servers
By the end of Q1 2021 you will be able to rent a virtual machine with 512Mb, 1024Mb or 2048Mb of RAM, this server will only be accessible via a .onion. By default it will have ports 22, 80 and 443 mapped.

You will have root access (via [doas](https://man.openbsd.org/doas) by default).

The server can have outbound IPv6 Internet access or be configured for [Quad Hop](/posts/what-is-quad-hop/).

## Free Multihop Only Web Hosting

For every person who created an [Ablative.Website](https://ablative.website) .onion we had **eight** ask for a .onion only version.

We've listened and Multihop Only Web Hosting will be added to [Ablative.Website](https://ablative.website).

We'll also be making PHP and MySQL available to all Ablative.Website accounts.


## Bring Your Own .Onion [Beta]

Since day 1 we have advised against giving us the private keys for your .onion. This advice has not changed.

What has changed is that [OnionBalance now supports v3 .onion's!](https://onionbalance.readthedocs.io/en/latest/v3/tutorial-v3.html).

You can opt to configure your Ablative Multihop VPS as an OnionBalance backend. You will need to run OnionBalance elsewhere but we
will never have access to the private keys for your .onion but we will be hosting it!

We will also be making it easier for people to import their own .onion without needing to use OnionBalance but shall continue to recommend against it.

---
## We want your feedback

The team would _really_ appreciate some feedback on these changes so please drop by our IRC server `irc://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion:6667/ablative`,
send an email to sales@ablative.hosting or send a Tweet to [@AblativeHosting](https://twitter.com/ablativehosting).

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: sales@ablative.hosting

xsBNBFlmO/oBCADCyWI3MvYDuoC53yc52iJGS01d+y89cIMlW8b4QGmOyOhj
rmXsgbbaB0keifxQ5ejpYEYru5JTvnn+4iDd061QGLoxrvPH6tovkbfZv/69
aL8m4pG+GTcW3FqhyQtktr5cJvN0iRgx+ulRtdqb37kTfiiPlEbkhNZbI6My
S5lNQaX7480BxmM3+6lL2fQkDX26L1X/caPq01yy2Zc8nvPfXjbIEen9m9PU
ZKXkXaxxXF8NqKSkulo+AAXfGW/CUn5xdgs1G1D4P1kkhEmIy4VlTnc3onoJ
TWia6c3tf5XdmBeEtGfKMohm1izqZq3yurrm1e1V00IY1MapLANXyibhABEB
AAHNL3NhbGVzQGFibGF0aXZlLmhvc3RpbmcgPHNhbGVzQGFibGF0aXZlLmhv
c3Rpbmc+wsB/BBABCAApBQJZZjv7BgsJBwgDAgkQ8vMv55C61XkEFQgKAgMW
AgECGQECGwMCHgEACgkQ8vMv55C61Xl1Bwf+N4fCmYbo9/RlVLoFkwyi+LGR
tewS1j1B+xeL+B80zRF23SnguFkcvLp8y2V/HxZibibMIJ5svbL25RlrG959
AXsjJ55chdlvILYQjgt304sKnjEf9CU+IQy/GdPz0jbtHkrkntjuqD2Dfiso
LlSLG+CcaK/yCXvKmbXG1pceTY/VhntGgoJiKJTOHn7FyRn8eUEhIB6NZHjI
paawtHiFefCTYJ8n5anZ53L6ZxJC5rQbba0HljsDkMZXVAs6CwQb2SfTvnL9
Vg/M4VbNmH3yEtsG4UBh7CawWOzvtze9Juma8NSZfJG6tLpM7Qvj33GJYaPa
EJ9JNrhOWkrE5MXAic7ATQRZZjv6AQgAupA3eh7bHII4M2sOsbnAEUPl2MX9
yydztkpH4WVesPR7GwM4MH/oMX01uCq6N9nUo+63rN/CL+xLYXWxYiZ4sI4+
e7LxrHRP6ng4IaknuBcltxYWsL6OyrhWYudQ1td72aZQmOeuT7xLQ9B3WEdQ
o1Gs3NUhVTNiYlY/CT5MR1oECtnt/DIF6co/Icw23PVhYXSG0e+jxux7VcZb
GB9iltJ65yBBME0bQf66LAQ/NCK7ck/nNc3lGmHd7s/IkiUwvzsGn0TkuDs+
1Gu+O07Rr03r0xeRXfLFefg8JsbVbuNbKo233qqm+mPNFH9owjGaSTpv5L4Q
+RsLZLrO469MoQARAQABwsBpBBgBCAATBQJZZjv7CRDy8y/nkLrVeQIbDAAK
CRDy8y/nkLrVeb9FB/9q98wOYJTl1p5OKauxLFL25QFUQ2D2ftLGXtgobXvl
iGvHJRFzqDyMSxE4SCYcwJz7I0tOtQ6wW5EG7tQifZqri1iuzrtcUx+HyPcE
F21RlGg1tKYlJ3WYXFScKnjVFcKWjuHSOcJ2SX1w07L4JuqAtYhwYeGKNxef
STISoym0vVciBbl7iUv7tbu5wFwi3EESLAjkjqPitP4UQ4O31Wy6HMFY4Jrp
ajgiu19pBQI7lsPRz8HlZH3+MQdC9dbQq3GLf+KQ4gurWN2Yw2fjMsVw21w5
s8ejYpJ3YaiYL4qkTp4SmRQHLwcQNQRu99WxdAdpZEhZE3lU7njNdT1DxrHQ
=y3B6
-----END PGP PUBLIC KEY BLOCK-----
```
