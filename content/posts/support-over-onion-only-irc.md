---
title: "Support via .onion Only IRC"
date: 2020-04-05T00:00:01
tags:
- irc
- .onion
- support
---
Did you know that Ablative.Hosting has a .onion only IRCd?

Available at **irc.hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/6667** this
IRC server is available for anyone interested in .onion technology whether they are a customer or not.

There's usually one or two members of staff luking in #ablative and we'll happily help you with any
.onion technical queries without a sales pitch! 

We don't support services such as NickServ or ChanServ at the moment _(mainly as we're only
expecting people to drop in to ask questions / get support for a problem rather than spend
any sustained period of time here)_. This may change if people start using the service more heavily.

We strongly recommend that people **never** provide any identifying details in the channel or even
over PM as whilst we've reserved `*ablative*` nicks there's always the chance that someone is
impersonating a staff member _(everyone's got to sleep some time)_.
