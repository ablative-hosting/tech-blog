---
title: "Introducing Ablative.Website - Free .Onion Hosting from Ablative.Hosting"
date: 2020-05-01T00:00:01
---

With the [sad loss of Daniel's Hosting](https://www.zdnet.com/article/dark-web-hosting-provider-hacked-again-7600-sites-down/) the
.onion hosting ecosystem has lost a home for ~7600 sites.

Whilst Ablative.Hosting is a commercial venture it is one with the aim of assisting people to resist Internet censorship.

If you can't afford a VPS, an always on Internet connection, a domain or our MHO512 service then the lack of access to finance is a form of censorship.

Free shouldn't mean co-operation with surveillance capitalism machinery, Free shouldn't mean having to hand over personal details or content rights.

---

So, with this in mind we're offering 128Mb of SSD disk space, a dedicated Gen3 .onion and a LetsEncrypt SSL enabled subdomain of your choice under [Ablative.Website](https://ablative.website) for **free**.

At launch these accounts can only host static content _(so no Wordpress or vBulletin forums etc)_ but if you need to tell the world something and want to offer your visitors the privacy, anonymity and censorship resistance of .onion without having to hand over any personal information about yourself _and_ get a clearnet mirror then we can help.

## Reconciling 'Free'
In today's Internet there's a maxim "If you're not paying; you're the product".

Every 'free' website has the potential to raise the profile of Ablative.Hosting, the more people who know about us the more chances we have of someone taking on a paid product.

That's it.

Our [Privacy Policy](https://ablative.hosting/privacy-policy/) prohibits us from sharing any of your data, you can sign-up via [our .onion website](http://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/) and the _only_
information we need from you is an SSH key and a password.

If you'd like to try it out then [sign up](https://ablative.hosting/signup/) ([.onion](http://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/signup/)) and then select the 'Free Shared Hosting Package' on our [Shared Single Hop](https://ablative.hosting/shared-single-hop-onion-hosting/) ([.onion](http://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/shared-single-hop-onion-hosting/)) product page.

## Please consider hosting your .onion yourself
The true power of the Internet is realised when everything is decentralised, that is to say the machine you are using to read this blog is probably powerful enough to host whatever you'd
publish on our free service.

By running a .onion yourself you maintain control of the Tor private keys, you control the computer and you alone have control over the content.

Reddit's [/r/onions](https://reddit.com/r/onions) is a good place to get help on running a .onion yourself, the [Raspberry PI is a capable enough device](https://magpi.raspberrypi.org/articles/tor-router) and there are plenty of [open source options](https://github.com/micahflee/onionshare/wiki/Website-Mode) to help you on your way.

You're more than welcome to be a customer but the true power of the Internet and .onion is to be your own host!
