---
title: "Onion Headers - A Way to Test .onion's for HTTP Security Headers"
date: 2020-03-01T00:00:01Z
---

HTTP headers can greatly improve the security of a website and in the cases of
`Content-Security-Policy` or `Feature-Policy` can greatly improve the security and
privacy of users too.

Just because a website is hosted on a .onion rather than .com doesn't mean webmasters
shouldn't make use of this security functionality.

Inspired by [OnionScan](https://onionscan.org/) from [OpenPriv](https://openprivacy.ca/)'s [Sarah Jamie Lewis](https://openprivacy.ca/people/sarah-jamie-lewis/)
Ablative Hosting's [https://onionheaders.website/](https://onionheaders.website/) intends to help .onion webmasters improve their httpd security configuration.

## Headers we are scanning for

### Content-Security-Policy
Content Security Policy (CSP) is an added layer of security that helps to detect and mitigate certain types of attacks, including Cross Site Scripting (XSS) and data injection attacks.
#### .onion Specific Requirement

By enabling CSP a .onion operator can prevent content being rendered if it is fetched from an unauthorized source, prevent javascript from executing (regardless of NoScript settings) and when coupled with subresource integrity hashes for CSS content etc (preferably served from a different .onion and server) it makes adding malicious Javascript to a .onion much harder.

### Feature-Policy
Feature Policy allows web developers to selectively enable, disable, and modify the behavior of certain features and APIs in the browser. It is similar to Content Security Policy but controls features instead of security behavior.
#### .onion Specific Requirement

By forbidding access to APIs such as the battery status or GeoLocation a .onion website can better protect users who aren't using the Tor Browser bundle.

### X-Frame-Options
The X-Frame-Options HTTP response header can be used to indicate whether or not a browser should be allowed to render a page in a <frame>, <iframe>, <embed> or <object>
#### .onion Specific Requirement

Helps prevent certain attacks by those who might register a similar .onion and display the real .onion within the frame or embed object

### X-Content-Type-Options
The X-Content-Type-Options response HTTP header is a marker used by the server to indicate that the MIME types advertised in the Content-Type headers should not be changed and be followed.
#### .onion Specific Requirement

Could be useful in certain situations where you want finer grained control over what a browser does if it is instructed to render a style but the type is not text/plain for example.

### X-XSS-Protection
The HTTP X-XSS-Protection response header is a feature of Internet Explorer, Chrome and Safari that stops pages from loading when they detect reflected cross-site scripting (XSS) attacks.
#### .onion Specific Requirement

Pretty self explanatory, in the event an attacker manages to trigger an XSS attack (and the .onion isn't preventing such execution by way of Content-Security-Policy) then the page will stop loading rather than risk the XSS firing.

### Referrer-Policy
The Referrer-Policy HTTP header controls how much referrer information (sent via the Referer header) should be included with requests.
#### .onion Specific Requirement

Thanks to version3 of the Hidden Service spec .onions can no longer be scraped from the HSDirs, when coupled with Client Authorisation a .onion may be totally undetectable to an adversary. By ensuring your .onion doesn't leak referrer information when a user clicks a link to another site (or embeds a resource) the existence of your .onion should remain confidential.

## Content MetaData we are scanning for
Many guides will advise users to disable Javascript when browsing .onion websites
however that advice is not always needed or useful. The scanner will _(eventually)_ scan for `<script>` tags and warn about their existance / if they are in violation of the CSP.

## Top Scoring Onions
We aren't currently recording any tests and will make it clear once we do _(any retention will be solely for the purposes of maintaining a scoreboard for gamification)_.
