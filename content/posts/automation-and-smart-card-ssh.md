---
title: "Automation and Smart Card Protected SSH"
date: 2020-02-11T10:36:11Z
tags:
- security
- ablative
- data
---

Ablative makes heavy use of [Ansible](https://ansible.com), this allows us to push 
changes to servers quickly, ensure that all instances are hardened appropriately
and, by following a secure software development lifecycle, we can automate all 
interaction with customer servers meaning no human (beyond the customer themselves) 
ever has access to a server.

One of the biggest issues with automation is protecting the credentials that
grant access to the customer servers. We've seen [APTs target MSPs](https://www.theregister.co.uk/2017/04/04/chinese_hackers_target_msps/)
and we can't ignore the possibility that the UK Government can [interfere with
our equipment](https://www.bigbrotherwatch.org.uk/wp-content/uploads/2016/03/Equipment-Interference.pdf)
or even seize our infrastructure.

We can't protect the SSH keys with passwords as that requires a human to interact
with the session.

So we turned to HSMs, namely the [NitroKey HSM 2](https://shop.nitrokey.com/shop/product/nitrokey-hsm-2-7).

## Hardware Security Modules

The primary benefit of a HSM is that the private key material is stored in an
area of memory that prevents extraction, this security mechanism is similar to
the 'secure enclave' in Apple products that [annoys the FBI et al](https://www.wired.com/2016/02/apples-fbi-battle-is-complicated-heres-whats-really-going-on/).

Using a HSM we can securely generate the private keys that allow Ansible to
access servers without the risk that someone could steal the keys from `~/.ssh/`.

The HSM is exposed via GPG agent so in theory someone with a shell on the automation
hosts _could_ still use the key. To thwart this we have several layered defences;
* No direct inbound connectivity
* Automation hosts use ansible-pull
* Tripwire logging to Splunk
* Syslog pushing to Splunk
* Signed git commits

## Layered Defence
### No direct inbound connectivity
Our automation servers are physically separate from the rest of the Ablative Infrastructure
and use both physical and host based firewalls to prevent inbound access via normal means.

These servers _do_ allow SSH access but only via an authenticated v3 .onion address.

In the event the servers become unreachable then someone from the team must travel
to the location and login locally.

### Automation Hosts Use Ansible Pull
[ansible-pull](https://docs.ansible.com/ansible/latest/cli/ansible-pull.html) inverts the
usual paradigm where the management host connects over SSH to targets and runs the
Ansible playbook. Instead the host pulls a copy of the playbooks _(including the ability to
leverage `verify-commit` (see 'Signed git commits' below) to ensure the code is authentic)_ and executes them
locally.

### Tripwire logging to Splunk
[TripWire](https://wwwtripwire.com) is a software suite that monitors a host for
any unauthorized changes to the filesystem. If any files are changed _(whether by
ansible-pull or not)_ a notification is sent to Splunk which in turn alerts the SOC
that something untoward is happening.

### Syslog pushing to Splunk
Syslog gathers a lot of the day-to-day activity of the server _(daemon lifecycle, crontabs,
etc)_ alongside important events such as a user logging in or a `sudo`/`doas` command
being issued.

These events are relayed to Splunk which can evaluate them and if neccessary raise an
alert to the SOC.

### Signed git commits
The automation hosts perform a `git pull` before they perform their scheduled actions,
if any new commits have been added since they last run the cycle through the commits
with `git verify-commit <hash>`.

If any of the commits fail GPG verification the scheduled tasks fails and an alert is
raised to the SOC.


## Physical Seizure
The last weakness in our model is if the automation servers and their HSMs are
seized as part of a Search and Seizure or Equipment Interference Warrant.

All servers utilise the [Brass Horn Communications S53 hardware](https://brasshorncommunications.uk/projects/s53/), in
the event that the server is moved, a USB device is plugged in _(and a variety of other conditions)_ then the s53 daemon
will lock the HSM, [destroy any Full Disk Encryption elements](https://github.com/BrassHornCommunications/s53/blob/master/Client/scripts/trigger/fedora.sh#L4) and, 
if necessary, shut the machine down.

---

We've gone to some effort to ensure that we can automate the management of 
customer servers without risking access by unauthorized parties, if this sort
of security is what you're looking for in a web host then drop by our [website](https://ablative.hosting) or
the [.onion](https://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/) and grab some
[shared hosting](https://ablative.hosting/shared-single-hop-onion-hosting/) or a [MultiHop VPS](https://ablative.hosting/multi-hop-onion-hosting/).
