---
title: "What is Ablative's Quad Hop?"
date: 2020-08-01T00:00:01Z
tags:
- quadhop
- .onion
- multihop
---
QuadHop is mentioned several times on our website but we've never explained what QuadHop _is_.

If you are unfamiliar with how .onion services interact with rendezvous points and directories then it would be a good idea
to check the Tor Project's [.onion service explainer](https://community.torproject.org/onion-services/overview/) first.

---

Most operators will run the Tor daemon on the same server as their service daemon (e.g. nginx) and may make mistakes that
allow the server to expose the IP addresses on other interfaces, expose fingerprints (e.g. SSH keys) that can be crawled on
the clearnet.

A QuadHop setup runs the Tor daemon on a server that has access to the Internet _and_ a firewalled LAN. The service daemon
resides on a server in the LAN. That server does not have public IP addresses, it cannot call out to the Internet, it is marooned.

```
                 PUBLIC                     UNROUTED 
                INTERNET                      LAN                    
+-------------+         +-----------------+           +------------+
| Tor Network | ------- | Quad Hop Server |---------- | Web Server |
+-------------+         +-----------------+           +------------+
```

The fourth hop in QuadHop is between the Tor bastion and the isolated server.

Ablative.Hosting runs on bare metal on our own ASN, there's no risk of a cloud provider being able to sniff the traffic content
between the Tor node and the service node.

Because QuadHop requires custom provisioning _(server hardware, switch ports etc)_ it is neither cheap nor available as a turnkey / month-to-month offering.

If you'd like to know more please send an email to sales@ablative.hosting or drop by IRC; irc://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion:6667

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: sales@ablative.hosting

xsBNBFlmO/oBCADCyWI3MvYDuoC53yc52iJGS01d+y89cIMlW8b4QGmOyOhj
rmXsgbbaB0keifxQ5ejpYEYru5JTvnn+4iDd061QGLoxrvPH6tovkbfZv/69
aL8m4pG+GTcW3FqhyQtktr5cJvN0iRgx+ulRtdqb37kTfiiPlEbkhNZbI6My
S5lNQaX7480BxmM3+6lL2fQkDX26L1X/caPq01yy2Zc8nvPfXjbIEen9m9PU
ZKXkXaxxXF8NqKSkulo+AAXfGW/CUn5xdgs1G1D4P1kkhEmIy4VlTnc3onoJ
TWia6c3tf5XdmBeEtGfKMohm1izqZq3yurrm1e1V00IY1MapLANXyibhABEB
AAHNL3NhbGVzQGFibGF0aXZlLmhvc3RpbmcgPHNhbGVzQGFibGF0aXZlLmhv
c3Rpbmc+wsB/BBABCAApBQJZZjv7BgsJBwgDAgkQ8vMv55C61XkEFQgKAgMW
AgECGQECGwMCHgEACgkQ8vMv55C61Xl1Bwf+N4fCmYbo9/RlVLoFkwyi+LGR
tewS1j1B+xeL+B80zRF23SnguFkcvLp8y2V/HxZibibMIJ5svbL25RlrG959
AXsjJ55chdlvILYQjgt304sKnjEf9CU+IQy/GdPz0jbtHkrkntjuqD2Dfiso
LlSLG+CcaK/yCXvKmbXG1pceTY/VhntGgoJiKJTOHn7FyRn8eUEhIB6NZHjI
paawtHiFefCTYJ8n5anZ53L6ZxJC5rQbba0HljsDkMZXVAs6CwQb2SfTvnL9
Vg/M4VbNmH3yEtsG4UBh7CawWOzvtze9Juma8NSZfJG6tLpM7Qvj33GJYaPa
EJ9JNrhOWkrE5MXAic7ATQRZZjv6AQgAupA3eh7bHII4M2sOsbnAEUPl2MX9
yydztkpH4WVesPR7GwM4MH/oMX01uCq6N9nUo+63rN/CL+xLYXWxYiZ4sI4+
e7LxrHRP6ng4IaknuBcltxYWsL6OyrhWYudQ1td72aZQmOeuT7xLQ9B3WEdQ
o1Gs3NUhVTNiYlY/CT5MR1oECtnt/DIF6co/Icw23PVhYXSG0e+jxux7VcZb
GB9iltJ65yBBME0bQf66LAQ/NCK7ck/nNc3lGmHd7s/IkiUwvzsGn0TkuDs+
1Gu+O07Rr03r0xeRXfLFefg8JsbVbuNbKo233qqm+mPNFH9owjGaSTpv5L4Q
+RsLZLrO469MoQARAQABwsBpBBgBCAATBQJZZjv7CRDy8y/nkLrVeQIbDAAK
CRDy8y/nkLrVeb9FB/9q98wOYJTl1p5OKauxLFL25QFUQ2D2ftLGXtgobXvl
iGvHJRFzqDyMSxE4SCYcwJz7I0tOtQ6wW5EG7tQifZqri1iuzrtcUx+HyPcE
F21RlGg1tKYlJ3WYXFScKnjVFcKWjuHSOcJ2SX1w07L4JuqAtYhwYeGKNxef
STISoym0vVciBbl7iUv7tbu5wFwi3EESLAjkjqPitP4UQ4O31Wy6HMFY4Jrp
ajgiu19pBQI7lsPRz8HlZH3+MQdC9dbQq3GLf+KQ4gurWN2Yw2fjMsVw21w5
s8ejYpJ3YaiYL4qkTp4SmRQHLwcQNQRu99WxdAdpZEhZE3lU7njNdT1DxrHQ
=y3B6
-----END PGP PUBLIC KEY BLOCK-----
```
