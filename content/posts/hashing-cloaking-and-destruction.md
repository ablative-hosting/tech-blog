---
title: "Hashing, Cloaking and Destruction - Data Management at Ablative.Hosting"
date: 2020-01-01T00:00:01Z
tags:
- security
- ablative
- data
---
At Ablative we make an effort to minimize the amount of data we need to store
about our customers _and_ the amount of meta-data needed to manage the service.

As we use Cryptocurrency instead of taking card payments we don't need to be
PCI compliant but we are concerned that there is a meta-data trail between
every Bitcoin transaction _(and Monero transaction if an adversary acquires your
private keys)_ and you.

We need to balance being able to process data _(e.g. we can't check if a bitcoin
address has received any satoshi's if we can't **see** the address)_ and
protecting a customer's anonymity.

Data within the Ablative platform has a life cycle that can result in it being;
* In native form
* Hashed
* Cloaked
* Destroyed

The data life cycle is directly related to the life cycle of an order and a server;
* New / Waiting for Payment
* Provisioning
* Active
* Pending decomission
* Terminated

___
**Before continuing it should be stressed that _all_ data we store is encrypted
at rest, encrypted during transport and in some cases is also wrapped with 
HSM protected AES256.**
___

## Hashing and UUIDs
Some data is stored in hashed form, for sensitive data _(e.g. passwords)_ we
utilise bcrypt.

For less sensitive data _(e.g. unique 'order numbers' or server identifiers)_
we use SHA256.

We also make use of [UUID's](https://en.wikipedia.org/wiki/Universally_unique_identifier) which,
whilst not a hash, provide a virtual guarantee of uniqueness, the sharp eyed
among you will have noticed that your user-name is a UUID.

## Native Form
During the life cycle of an order or a server we need to be able to process data
such as the bitcoin or monero address.

Before an order has been paid there is no risk to an identity as no transaction
has occured so the bitcoin address can exist in native form.

Once a payment has been received the system will 'cloak' data that will no longer
processed by software.

## Cloaking

Cloaking is simply our way of describing PGP encrypting the data. If data was
stolen from the database _(or we were instructed to hand data over via a warrant)_
the data would be unusable to anyone without the private keys.

So why do we 'cloak' data instead of just destroying it immediately?

There is a period of time between an order having been paid and a customer
server becoming fully operational. During this time it is possible that our
SREs or Support Staff will need to triage a problem.

Having access to the meta data of an order _(e.g. bitcoin address, internal
transaction UUIDs etc)_ can be critical to diagnosing and resolving issues.

Once a server has been commisioned we would no longer need this data, it is
at this point that we would destroy it.

Examples of cloaked data are;
* A cryptocurrency address after an order has been paid but the server has yet to be commisioned
* Meta data linking an order to a cryptocurrency address _(a 'transaction ID')_
* Meta data linking a server to an order _(An 'order ID')_

## Destruction

Data that is no longer relevant is deleted from the relevant data storage
medium, in 99% of cases that means we replace the data with a blank string
in the MariaDB column.

A few examples of destruction are;

* Cryptocurrency addresses are 'destroyed' once a server is successfully provisioned
* Order meta data _(User #123's order ID #456 resulted in SHO1024 server #789)_ is destroyed when a server is terminated
* Virtual Server disks are destroyed the moment a server is terminated
* User details _(password hash, email, pgp key etc)_ are destroyed when a user clicks the 'Delete Account' button

# The Paradox of Backups

Without backups we can't restore the platform in the event of a disaster.

With backups an adversary has access to 'snapshots' in time where data might
be in native form, hashed or cloaked.

With this in mind we protect our backups in several ways;
* Backups are encrypted before they leave the server
  - We use public key cryptography with the keys being held in a HSM
  - The HSM PINs are only known by the Managing Director
* Hot backups _(day of week)_ rotate rapidly 
* Cold backups _(monthly)_ are escrowed 
